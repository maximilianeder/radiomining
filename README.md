# RadioMining

Here is the data repository for a small university project analyzing the behaviour of some big (bavarian) radio stations. 
The data is not openly available (for copyright reasons) but my code and the generated graphs are here together with my presentation.

## Support
The project is finished so there is no support needed. Full credit to @maximilian.haberl and @lorenznickel for data collection and presentation design.
